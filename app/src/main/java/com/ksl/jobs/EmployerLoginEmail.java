package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class EmployerLoginEmail extends AppCompatActivity {

    TextInputLayout emplrEmail, emplrPassword;
    Button loginButton, phoneButton;
    TextView forgotPwd, signUptext;
    FirebaseAuth firebaseAuth;
    String eEmail, ePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_login_email_test);

        try {
            emplrEmail = findViewById(R.id.emplrEmail);
            emplrPassword = findViewById(R.id.emplrPwd);
            loginButton = findViewById(R.id.loginBtn);
            phoneButton = findViewById(R.id.btnphone);
            forgotPwd = findViewById(R.id.forgotPwd);
            signUptext = findViewById(R.id.signupText);

            firebaseAuth = FirebaseAuth.getInstance();

            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eEmail = emplrEmail.getEditText().getText().toString().trim();
                    ePassword = emplrPassword.getEditText().getText().toString().trim();

                    if(isValid()){
                        final ProgressDialog mDialog = new ProgressDialog(EmployerLoginEmail.this);
                        mDialog.setCanceledOnTouchOutside(false);
                        mDialog.setCancelable(false);
                        mDialog.setMessage("Please wait.....");
                        mDialog.show();

                        firebaseAuth.signInWithEmailAndPassword(eEmail, ePassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (task.isSuccessful()){
                                    mDialog.dismiss();

                                    if(firebaseAuth.getCurrentUser().isEmailVerified()){
                                        mDialog.dismiss();
                                        Toast.makeText(EmployerLoginEmail.this, "You are logged in!", Toast.LENGTH_SHORT).show();
                                        Intent z = new Intent(EmployerLoginEmail.this, EmployerPanel_BottomNav.class);
                                        startActivity(z);
                                        finish();
                                    } else{
                                      ReusableCodeforAll.showAlert(EmployerLoginEmail.this, "Verification Failed", "Please verify your email");
                                    }
                                } else{
                                    mDialog.dismiss();
                                    ReusableCodeforAll.showAlert(EmployerLoginEmail.this, "Error", task.getException().getMessage());

                                }
                            }
                        });
                    }
                }
            });

            signUptext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(EmployerLoginEmail.this, EmployerRegister.class));
                    finish();
                }
            });

            forgotPwd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(EmployerLoginEmail.this, EmployerForgotPassword.class));
                    finish();
                }
            });

            phoneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(EmployerLoginEmail.this, EmployerLoginPhone.class));
                    finish();
                }
            });
        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

        }

    }

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    public boolean isValid(){

        emplrEmail.setErrorEnabled(false);
        emplrEmail.setError("");

        emplrPassword.setErrorEnabled(false);
        emplrPassword.setError("");

        boolean isValid = false, isValidEmail = false, isValidPwd = false;
        if(TextUtils.isEmpty(eEmail)){
            emplrEmail.setErrorEnabled(true);
            emplrEmail.setError("Email id is required");
        }else{
            if(eEmail.matches(emailPattern)){
                isValidEmail = true;
            } else{
                emplrEmail.setErrorEnabled(true);
                emplrEmail.setError("Invalid Email");
            }
        }

        if(TextUtils.isEmpty(ePassword)){
            emplrPassword.setErrorEnabled(true);
            emplrPassword.setError("Password Required");
        }
        else {
            isValidPwd = true;
        }

        isValid = (isValidEmail && isValidPwd)? true : false;
        return isValid;
    }
}