package com.ksl.jobs.userPanel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.ksl.jobs.R;
import com.ksl.jobs.UpdateJobModel;

import java.util.List;

public class UserHomeAdapter extends RecyclerView.Adapter<UserHomeAdapter.ViewHolder> {

    private Context mcontext;
    private List<UpdateJobModel> updateJobModellist;
    DatabaseReference databaseReference;

    public UserHomeAdapter(Context context , List<UpdateJobModel>updateJobModelslist){

        this.updateJobModellist = updateJobModelslist;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public UserHomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.user_menu_job,parent,false);
        return new UserHomeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserHomeAdapter.ViewHolder holder, int position) {

        final UpdateJobModel updateJobModel = updateJobModellist.get(position);
        Glide.with(mcontext).load(updateJobModel.getImageUrl()).into(holder.imageView);
        holder.jobname.setText(updateJobModel.getSalary());
        updateJobModel.getRandomUid();
        updateJobModel.getEmplrId();
        holder.salary.setText("Salary: "+updateJobModel.getSalary()+"Rs");

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView jobname,salary;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.job_image);
            jobname = itemView.findViewById(R.id.jobname);
            salary = itemView.findViewById(R.id.jobsalary);
        }
    }
}
