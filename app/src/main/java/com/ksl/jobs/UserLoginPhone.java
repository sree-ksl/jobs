package com.ksl.jobs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.hbb20.CountryCodePicker;

public class UserLoginPhone extends AppCompatActivity {

    EditText userNumber;
    Button sendOtp, emailButton;
    TextView signUp;
    CountryCodePicker cpp;
    FirebaseAuth firebaseAuth;
    String uNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login_phone_test);

        userNumber = findViewById(R.id.userNumber);
        sendOtp = findViewById(R.id.uotp);
        emailButton = findViewById(R.id.ubtnEmail);
        signUp = findViewById(R.id.usignup);
        cpp = findViewById(R.id.CountryCode);

        firebaseAuth = FirebaseAuth.getInstance();

        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uNumber = userNumber.getText().toString().trim();
                String phoneNum = cpp.getSelectedCountryCodeWithPlus()+uNumber;
                Intent b = new Intent(UserLoginPhone.this, UserSendOtp.class);
                b.putExtra("uPhoneNum", phoneNum);
                startActivity(b);
                finish();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserLoginPhone.this, UserRegister.class));
                finish();
            }
        });

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserLoginPhone.this, UserLoginEmail.class));
                finish();
            }
        });

    }
}