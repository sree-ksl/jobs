package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ksl.jobs.employerPanel.EmployerHomeFragment;
import com.ksl.jobs.employerPanel.EmployerInterviewsFragment;
import com.ksl.jobs.employerPanel.EmployerPostJobFragment;
import com.ksl.jobs.employerPanel.EmployerRequestsFragment;
import com.ksl.jobs.userPanel.UserHomeFragment;

public class EmployerPanel_BottomNav extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_panel__bottom_nav);

        BottomNavigationView navigationView = findViewById(R.id.employerBottomNav);
        navigationView.setOnNavigationItemSelectedListener(this);

//        String name = getIntent().getStringExtra("PAGE");
//        if(name != null){
//            if(name.equalsIgnoreCase("Homepage")){
//                loadEmployerFragment(new EmployerHomeFragment());
//            }
//
//        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.emplrHome:
                fragment = new EmployerHomeFragment();
                break;
            case R.id.emplrIntRequests:
                fragment = new EmployerRequestsFragment();
                break;
            case R.id.emplrInterviews:
                fragment = new EmployerInterviewsFragment();
                break;
            case R.id.emplrPostJob:
                fragment = new EmployerPostJobFragment();
                break;
        }

        return loadEmployerFragment(fragment);
    }

    private boolean loadEmployerFragment(Fragment fragment) {

        if(fragment != null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
            return true;
        }
        return false;
    }
}