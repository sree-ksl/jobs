package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.arch.core.executor.TaskExecutor;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class EmployerVerifyPhone extends AppCompatActivity {

    String verificationId;
    FirebaseAuth firebaseAuth;
    Button verifyButton, resendOtpButton;
    TextView text;
    EditText otpCode;
    String phoneNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_verify_phone);

        phoneNum = getIntent().getStringExtra("phonenumber").trim();

        otpCode = findViewById(R.id.codeOtp);
        text = findViewById(R.id.text);
        verifyButton = findViewById(R.id.verifyBtn);
        resendOtpButton = findViewById(R.id.resendotpBtn);
        firebaseAuth = FirebaseAuth.getInstance();

        resendOtpButton.setVisibility(View.INVISIBLE);
        text.setVisibility(View.INVISIBLE);

        sendVerificationCode(phoneNum);


        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = otpCode.getText().toString().trim();
                resendOtpButton.setVisibility(View.INVISIBLE);

                if(code.isEmpty() && code.length()<6){
                    otpCode.setError("Enter code");
                    otpCode.requestFocus();
                    return;
                }
                verifyCode(code);
            }
        });

        new CountDownTimer(60000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

                text.setVisibility(View.VISIBLE);
                text.setText("Resend Code Within "+millisUntilFinished/1000+" Seconds");

            }

            /**
             * Callback fired when the time is up.
             */
            @Override
            public void onFinish() {
                resendOtpButton.setVisibility(View.VISIBLE);
                text.setVisibility(View.INVISIBLE);

            }
        }.start();


        resendOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOtpButton.setVisibility(View.INVISIBLE);
                resendOTP(phoneNum);

                new CountDownTimer(60000, 10000){

                    @Override
                    public void onTick(long l) {
                        text.setVisibility(View.VISIBLE);
                        text.setText("Resend code within " + l/1000 + "seconds");
                    }

                    @Override
                    public void onFinish() {
                        resendOtpButton.setVisibility(View.VISIBLE);
                        text.setVisibility(View.INVISIBLE);
                    }
                }.start();
            }
        });
    }

    private void resendOTP(String phoneNum) {
        sendVerificationCode(phoneNum);
    }

    private void sendVerificationCode(String number) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mcallback
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mcallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null){
                otpCode.setText(code);  //AUto verification
                verifyCode(code);
            }

        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(EmployerVerifyPhone.this, e.getMessage(), Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken){
            super.onCodeSent(s, forceResendingToken);

            verificationId = s;
        }
    };

    private void verifyCode(String code) {

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        linkCredential(credential);
    }

    private void linkCredential(PhoneAuthCredential credential) {

        firebaseAuth.getCurrentUser().linkWithCredential(credential).addOnCompleteListener(EmployerVerifyPhone.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()) {
                    Intent intent = new Intent(EmployerVerifyPhone.this, MainMenu.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    ReusableCodeforAll.showAlert(EmployerVerifyPhone.this, "Error", task.getException().getMessage());
                }

            }
        });
    }
}