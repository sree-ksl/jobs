package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;
import java.util.HashMap;

public class EmployerRegister extends AppCompatActivity {

    String AndhraPradesh[] = {"Vizag", "Vijayawada"};
    String Karnataka[] = {"Bangalore", "Mysore"};
    String Telangana[] = {"Hyderabad", "Secundrabad"};

    TextInputLayout emplrFirstName,emplrLastName,emplrEmail,emplrPassword,emplrConfirmPwd,emplrMobile,emplrCompany,emplrDoorNo,emplrArea,emplrPincode;
    Spinner emplrState,emplrCity;
    Button signup, email, phone;
    CountryCodePicker cpp;
    FirebaseAuth fAuth;
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;
    String eFirstName,eLastName,eEmail,ePassword,eConfirmPwd,eMobile,eCompany,eDoorNo,eArea,ePincode, eState, eCity;
    String role = "Employer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_register);

        emplrFirstName = findViewById(R.id.emplrFirstName);
        emplrLastName = findViewById(R.id.emplrLastName);
        emplrEmail = findViewById(R.id.emplrEmail);
        emplrPassword = findViewById(R.id.emplrPwd);
        emplrConfirmPwd = findViewById(R.id.emplrConfirmPwd);
        emplrMobile = findViewById(R.id.emplrMobile);
        emplrCompany = findViewById(R.id.emplrCompany);
        emplrDoorNo = findViewById(R.id.emplrStreet);
        emplrArea = findViewById(R.id.emplrArea);
        emplrPincode = findViewById(R.id.emplrPincode);
        emplrState = findViewById(R.id.stateSpinner);
        emplrCity = findViewById(R.id.citySpinner);

        signup = findViewById(R.id.emplrSignupBtn);
        email = findViewById(R.id.emailBtn);
        phone = findViewById(R.id.phoneBtn);

        cpp = findViewById(R.id.countryCode);

        emplrState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Object value = adapterView.getItemAtPosition(position);
                eState = value.toString().trim();
                if(eState.equals("AndhraPradesh")){
                    ArrayList<String> list = new ArrayList<>();
                    for (String cities : AndhraPradesh){
                        list.add(cities);
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EmployerRegister.this, android.R.layout.simple_spinner_item, list);
                    emplrCity.setAdapter(arrayAdapter);
                }
                if(eState.equals("Karnataka")){
                    ArrayList<String> list = new ArrayList<>();
                    for (String cities : Karnataka){
                        list.add(cities);
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EmployerRegister.this, android.R.layout.simple_spinner_item, list);
                    emplrCity.setAdapter(arrayAdapter);
                }
                if(eState.equals("Telangana")){
                    ArrayList<String> list = new ArrayList<>();
                    for (String cities : Telangana){
                        list.add(cities);
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EmployerRegister.this, android.R.layout.simple_spinner_item, list);
                    emplrCity.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        emplrCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Object value = adapterView.getItemAtPosition(position);
                eCity = value.toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference("Employer");
        fAuth = FirebaseAuth.getInstance();

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eFirstName = emplrFirstName.getEditText().getText().toString().trim();
                eLastName = emplrLastName.getEditText().getText().toString().trim();
                eEmail = emplrEmail.getEditText().getText().toString().trim();
                ePassword = emplrPassword.getEditText().getText().toString().trim();
                eConfirmPwd = emplrConfirmPwd.getEditText().getText().toString().trim();
                eMobile = emplrMobile.getEditText().getText().toString().trim();
                eCompany = emplrCompany.getEditText().getText().toString().trim();
                eDoorNo = emplrDoorNo.getEditText().getText().toString().trim();
                eArea = emplrArea.getEditText().getText().toString().trim();
                ePincode = emplrPincode.getEditText().getText().toString().trim();

                if (isValid()) {
                    final ProgressDialog mDialog = new ProgressDialog(EmployerRegister.this);
                    mDialog.setCancelable(false);
                    mDialog.setCanceledOnTouchOutside(false);
                    mDialog.setMessage("Registration in progress, please wait....");
                    mDialog.show();

                    fAuth.createUserWithEmailAndPassword(eEmail, ePassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()){
                                String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                databaseReference = FirebaseDatabase.getInstance().getReference("employer").child(userID);
                                final HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("Role", role);
                                databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        HashMap<String, String> hashMap1 = new HashMap<>();
                                        hashMap1.put("First Name", eFirstName);
                                        hashMap1.put("Last Name", eLastName);
                                        hashMap1.put("Emailid", eEmail);
                                        hashMap1.put("Password", ePassword);
                                        hashMap1.put("Confirm Password", eConfirmPwd);
                                        hashMap1.put("Mobile", eMobile);
                                        hashMap1.put("Company Name", eCompany);
                                        hashMap1.put("Door no", eDoorNo);
                                        hashMap1.put("Area", eArea);
                                        hashMap1.put("Pincode", ePincode);
                                        hashMap1.put("State", eState);
                                        hashMap1.put("City", eCity);

                                        firebaseDatabase.getInstance().getReference("Employer")
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                .setValue(hashMap1).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                mDialog.dismiss();

                                                fAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful()){
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(EmployerRegister.this);
                                                            builder.setMessage("Registration Successful. Please verify your Email");
                                                            builder.setCancelable(false);
                                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int which) {
                                                                    dialogInterface.dismiss();

                                                                    //phone num registration
                                                                    String phoneNumber = cpp.getSelectedCountryCodeWithPlus() + eMobile;
                                                                    Intent b = new Intent(EmployerRegister.this, EmployerVerifyPhone.class);
                                                                    b.putExtra("phonenumber", phoneNumber);
                                                                    startActivity(b);
                                                                }
                                                            });
                                                            AlertDialog alert = builder.create();
                                                            alert.show();
                                                        } else{
                                                            mDialog.dismiss();
                                                            ReusableCodeforAll.showAlert(EmployerRegister.this,"Error",task.getException().getMessage());
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                            }else{
                                mDialog.dismiss();
                                ReusableCodeforAll.showAlert(EmployerRegister.this,"Error",task.getException().getMessage());
                            }
                        }
                    });
                }

            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EmployerRegister.this, EmployerLoginEmail.class));
                finish();
            }
        });

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EmployerRegister.this, EmployerLoginPhone.class));
                finish();
            }
        });

    }

    //check for invalid entries
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public Boolean isValid(){
        emplrEmail.setErrorEnabled(false);
        emplrEmail.setError("");
        emplrFirstName.setErrorEnabled(false);
        emplrFirstName.setError("");
        emplrLastName.setErrorEnabled(false);
        emplrLastName.setError("");
        emplrPassword.setErrorEnabled(false);
        emplrPassword.setError("");
        emplrConfirmPwd.setErrorEnabled(false);
        emplrConfirmPwd.setError("");
        emplrMobile.setErrorEnabled(false);
        emplrMobile.setError("");
        emplrCompany.setErrorEnabled(false);
        emplrCompany.setError("");
        emplrDoorNo.setErrorEnabled(false);
        emplrDoorNo.setError("");
        emplrArea.setErrorEnabled(false);
        emplrArea.setError("");
        emplrPincode.setErrorEnabled(false);
        emplrPincode.setError("");

        boolean isValid = false, isValidFirstname = false, isValidLastname = false, isValidemail = false, isValidPwd = false, isValidCpwd = false, isValidMobile = false, isValidCompany = false, isValidDoorno = false, isValidArea = false, isValidPincode = false;
        if(TextUtils.isEmpty(eFirstName)){
            emplrFirstName.setErrorEnabled(true);
            emplrFirstName.setError("Enter First Name");
        }
        else {
            isValidFirstname = true;
        }

        if(TextUtils.isEmpty(eLastName)){
            emplrLastName.setErrorEnabled(true);
            emplrLastName.setError("Enter Last Name");
        }
        else {
            isValidLastname = true;
        }

        if(TextUtils.isEmpty(eEmail)){
            emplrEmail.setErrorEnabled(true);
            emplrEmail.setError("Email id is required");
        }
        else {
            if(eEmail.matches(emailPattern)){
                isValidemail = true;
            } else{
                emplrEmail.setErrorEnabled(true);
                emplrEmail.setError("Enter valid email id");
            }

        }

        if(TextUtils.isEmpty(ePassword)){
            emplrPassword.setErrorEnabled(true);
            emplrPassword.setError("Enter Password");
        }
        else {
            if (ePassword.length()<8){
                emplrPassword.setErrorEnabled(true);
                emplrPassword.setError("Weak Password");
            }
            else{
                isValidPwd = true;
            }
        }

        if(TextUtils.isEmpty(eConfirmPwd)){
            emplrConfirmPwd.setErrorEnabled(true);
            emplrConfirmPwd.setError("Enter password again");
        } else {
            if(!ePassword.equals(eConfirmPwd)){
                emplrConfirmPwd.setErrorEnabled(true);
                emplrConfirmPwd.setError("Mismatched password");
            }
            else{
                isValidCpwd = true;

            }
        }

        if(TextUtils.isEmpty(eMobile)){
            emplrMobile.setErrorEnabled(true);
            emplrMobile.setError("Enter Mobile number");
        }
        else {
            if(eMobile.length()<10){
                emplrMobile.setErrorEnabled(true);
                emplrMobile.setError("Invalid Mobile number");
            }
            else{
                isValidMobile = true;
            }
        }

        if(TextUtils.isEmpty(eCompany)){
            emplrCompany.setErrorEnabled(true);
            emplrCompany.setError("Enter Company Name");
        }
        else {
            isValidCompany = true;
        }

        if(TextUtils.isEmpty(eDoorNo)){
            emplrDoorNo.setErrorEnabled(true);
            emplrDoorNo.setError("Enter Door no/ Street");
        }
        else {
            isValidDoorno = true;
        }

        if(TextUtils.isEmpty(eArea)){
            emplrArea.setErrorEnabled(true);
            emplrArea.setError("Enter Area");
        }
        else {
            isValidArea = true;
        }

        if(TextUtils.isEmpty(ePincode)){
            emplrPincode.setErrorEnabled(true);
            emplrPincode.setError("Enter Pincode");
        }
        else {
            isValidPincode = true;
        }

        isValid = (isValidFirstname && isValidLastname && isValidemail && isValidPwd && isValidCpwd && isValidCompany && isValidDoorno && isValidArea && isValidPincode && isValidMobile) ? true : false;
        return isValid;
    }
}