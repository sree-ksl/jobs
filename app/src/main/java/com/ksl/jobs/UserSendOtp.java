package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class UserSendOtp extends AppCompatActivity {

    String uverificationId;
    FirebaseAuth firebaseAuth;
    Button uverifyButton, uresendOtpButton;
    TextView utext;
    EditText uotpCode;
    String uphoneNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_send_otp);

        uphoneNum = getIntent().getStringExtra("uPhonenum").trim();

        uotpCode = findViewById(R.id.ucodeOtp);
        utext = findViewById(R.id.utext);
        uverifyButton = findViewById(R.id.uverifyBtn);
        uresendOtpButton = findViewById(R.id.uresendotpBtn);
        firebaseAuth = FirebaseAuth.getInstance();

        uresendOtpButton.setVisibility(View.INVISIBLE);
        utext.setVisibility(View.INVISIBLE);

        sendVerificationCode(uphoneNum);

        uverifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = uotpCode.getText().toString().trim();
                uresendOtpButton.setVisibility(View.INVISIBLE);

                if(code.isEmpty() && code.length()<6){
                    uotpCode.setError("Enter code");
                    uotpCode.requestFocus();
                    return;
                }
                verifyCode(code);
            }
        });

        new CountDownTimer(60000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

                utext.setVisibility(View.VISIBLE);
                utext.setText("Resend Code Within "+millisUntilFinished/1000+" Seconds");

            }

            /**
             * Callback fired when the time is up.
             */
            @Override
            public void onFinish() {
                uresendOtpButton.setVisibility(View.VISIBLE);
                utext.setVisibility(View.INVISIBLE);

            }
        }.start();

        uresendOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uresendOtpButton.setVisibility(View.INVISIBLE);
                resendOTP(uphoneNum);

                new CountDownTimer(60000, 10000){

                    @Override
                    public void onTick(long l) {
                        utext.setVisibility(View.VISIBLE);
                        utext.setText("Resend code within " + l/1000 + "seconds");
                    }

                    @Override
                    public void onFinish() {
                        uresendOtpButton.setVisibility(View.VISIBLE);
                        utext.setVisibility(View.INVISIBLE);
                    }
                }.start();
            }
        });
    }

    private void resendOTP(String phoneNum) {
        sendVerificationCode(phoneNum);
    }

    private void sendVerificationCode(String number) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mcallback
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mcallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null){
                uotpCode.setText(code);  //AUto verification
                verifyCode(code);
            }

        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(UserSendOtp.this, e.getMessage(), Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken){
            super.onCodeSent(s, forceResendingToken);

            uverificationId = s;
        }
    };

    private void verifyCode(String code) {

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(uverificationId, code);
        signInWithPhone(credential);
    }

    private void signInWithPhone(PhoneAuthCredential credential) {

        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            startActivity(new Intent(UserSendOtp.this, UserPanel_BottomNav.class));
                            finish();
                        }else{
                            ReusableCodeforAll.showAlert(UserSendOtp.this, "Error", task.getException().getMessage());
                        }
                    }
                });

    }
}