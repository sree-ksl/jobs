package com.ksl.jobs.employerPanel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ksl.jobs.R;
import com.ksl.jobs.UpdateJobModel;

import java.util.List;

public class EmployerHomeAdapter extends RecyclerView.Adapter<EmployerHomeAdapter.ViewHolder> {

    private Context mcont;
    private List<UpdateJobModel> updateJobModelList;

    public EmployerHomeAdapter(Context context , List<UpdateJobModel>updateJobModelList){
        this.updateJobModelList = updateJobModelList;
        this.mcont = context;
    }

    @NonNull
    @Override
    public EmployerHomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcont).inflate(R.layout.employer_job_update_delete,parent,false);
        return new EmployerHomeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployerHomeAdapter.ViewHolder holder, int position) {

        final UpdateJobModel updateJobModel = updateJobModelList.get(position);
        holder.jobs.setText(updateJobModel.getJobs());
        updateJobModel.getRandomUid();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mcont,UpdateDelete_Job.class);
                intent.putExtra("updatedeleteJob",updateJobModel.getRandomUid());
                mcont.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return updateJobModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView jobs;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            jobs = itemView.findViewById(R.id.job_name);
        }
    }

}
