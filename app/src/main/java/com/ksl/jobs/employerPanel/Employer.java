package com.ksl.jobs.employerPanel;

public class Employer {

    private String area, city, confirmPwd, emailid, fname, house, lname, mobile, password, postcode, state;

    public Employer(String area, String city, String confirmPwd, String emailid, String fname, String house, String lname, String mobile, String password, String postcode, String state) {
        this.area = area;
        this.city = city;
        this.confirmPwd = confirmPwd;
        this.emailid = emailid;
        this.fname = fname;
        this.house = house;
        this.lname = lname;
        this.mobile = mobile;
        this.password = password;
        this.postcode = postcode;
        this.state = state;
    }

    public Employer(){}

    public String getArea(){ return area; }

    public String getCity() { return city; }

    public String getConfirmPwd() { return confirmPwd; }

    public String getEmailid() { return emailid; }

    public String getFname() { return fname; }

    public String getHouse() { return house; }

    public String getLname() { return lname; }

    public String getMobile() { return mobile; }

    public String getPassword() { return password; }

    public String getPostcode() { return postcode; }

    public String getState() { return state; }
}
