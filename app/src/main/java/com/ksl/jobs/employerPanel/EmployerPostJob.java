package com.ksl.jobs.employerPanel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ksl.jobs.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.UUID;

public class EmployerPostJob extends AppCompatActivity {

    ImageButton imageButton;
    Button postJob;
    Spinner jobs;
    TextInputLayout  description, vacancies, salary;
    String desc, vac, sal, job;
    Uri imageUri;
    private Uri mCropImageUri;
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference,dataa;
    FirebaseAuth Fauth;
    StorageReference ref;
    String emplrID, randomUid, state, city, area;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_post_job);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        jobs = (Spinner)findViewById(R.id.jobName);
        description = findViewById(R.id.description);
        vacancies = findViewById(R.id.vacancies);
        salary = findViewById(R.id.salary);
        postJob = findViewById(R.id.post);
        Fauth = FirebaseAuth.getInstance();
        databaseReference = firebaseDatabase.getInstance().getReference("JobDetails");


        try {
            String userid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            dataa = firebaseDatabase.getInstance().getReference("Employer").child(userid);
            dataa.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    Employer employer = snapshot.getValue(Employer.class);
                    state = employer.getstate();
                    city = employer.getcity();
                    area = employer.getarea();
                    imageButton = findViewById(R.id.imageUpload);

                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onSelectImageclick(view);
                        }
                    });

                    postJob.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            job = jobs.getSelectedItem().toString().trim();
                            desc = description.getEditText().getText().toString().trim();
                            vac = vacancies.getEditText().getText().toString().trim();
                            sal = salary.getEditText().getText().toString().trim();

                            if(isValid()){
                                uploadImage();
                            }
                        }
                    });

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        catch (Exception e){
            Log.e("Error: ", e.getMessage());
        }
    }

    private void uploadImage() {
        if (imageUri != null){
            final ProgressDialog progressDialog = new ProgressDialog(EmployerPostJob.this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            randomUid = UUID.randomUUID().toString();
            ref = storageReference.child(randomUid);
            emplrID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            ref.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {

                            JobDetails info = new JobDetails(job, vac, sal, desc, String.valueOf(uri), randomUid, emplrID);
                            firebaseDatabase.getInstance().getReference("JobDetails").child(state).child(city).child(area)
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(randomUid)
                                    .setValue(info).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressDialog.dismiss();
                                    Toast.makeText(EmployerPostJob.this, "Job Posted", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(EmployerPostJob.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                    double progress = (100.0*snapshot.getBytesTransferred()/snapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploaded " + (int) progress+ "%");
                    progressDialog.setCanceledOnTouchOutside(false);
                }
            });
        }


    }

    private boolean isValid() {

        description.setErrorEnabled(false);
        description.setError("");
        vacancies.setErrorEnabled(false);
        vacancies.setError("");
        salary.setErrorEnabled(false);
        salary.setError("");

        boolean isValidDescription = false, isValidVacancies = false, isValidSalary = false, isValid = false;
        if(TextUtils.isEmpty(desc)){
            description.setErrorEnabled(true);
            description.setError("Required");
        }else{
            description.setError(null);
            isValidDescription = true;
        }

        if(TextUtils.isEmpty(vac)){
            vacancies.setErrorEnabled(true);
            vacancies.setError("Enter no. of Vanacies");
        }else{
//            vacancies.setError(null);
            isValidVacancies = true;
        }

        if(TextUtils.isEmpty(sal)){
            salary.setErrorEnabled(true);
            salary.setError("Enter salary range");
        }else{
//            salary.setError(null);
            isValidSalary = true;
        }
        isValid = (isValidDescription && isValidVacancies && isValidSalary)?true:false;
        return isValid;
    }

    private void startCropImageActivity(Uri imageuri){
        CropImage.activity(imageuri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }
    private void onSelectImageclick(View v){
        CropImage.startPickImageActivity(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(mCropImageUri !=null && grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
            startCropImageActivity(mCropImageUri);
        }else{
            Toast.makeText(this,"Cancelling! Permission Not Granted",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode==CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode== Activity.RESULT_OK){
            imageUri = CropImage.getPickImageResultUri(this,data);
            if(CropImage.isReadExternalStoragePermissionsRequired(this,imageUri)){
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},0);
            }else{
                startCropImageActivity(imageUri);
            }
        }
        if(requestCode==CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if(resultCode==RESULT_OK){
                ((ImageButton) findViewById(R.id.imageUpload)).setImageURI(result.getUri());
                Toast.makeText(this,"Cropped Successfully!",Toast.LENGTH_SHORT).show();
            }else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                Toast.makeText(this,"Failed To Crop"+result.getError(),Toast.LENGTH_SHORT).show();

            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}