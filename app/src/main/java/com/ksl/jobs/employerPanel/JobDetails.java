package com.ksl.jobs.employerPanel;

public class JobDetails {
    public String job, vacancies, salary, description, imageUrl, randomUid, emplrID;

    public JobDetails(String job, String vacancies, String salary, String description, String imageUrl, String randomUid, String emplrID) {
        job = job;
        vacancies = vacancies;
        salary = salary;
        description = description;
        imageUrl = imageUrl;
        randomUid = randomUid;
        emplrID = emplrID;
    }
}
