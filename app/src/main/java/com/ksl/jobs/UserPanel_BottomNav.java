package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ksl.jobs.employerPanel.EmployerHomeFragment;
import com.ksl.jobs.employerPanel.EmployerInterviewsFragment;
import com.ksl.jobs.employerPanel.EmployerPostJobFragment;
import com.ksl.jobs.employerPanel.EmployerRequestsFragment;
import com.ksl.jobs.userPanel.UserHomeFragment;
import com.ksl.jobs.userPanel.UserInterviewsFragment;
import com.ksl.jobs.userPanel.UserProfileFragment;
import com.ksl.jobs.userPanel.UserRequestsFragment;

public class UserPanel_BottomNav extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_panel_bottom_nav);
        BottomNavigationView navigationView = findViewById(R.id.userBottomNav);
        navigationView.setOnNavigationItemSelectedListener(this);

//        String name = getIntent().getStringExtra("PAGE");
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        if(name!=null){
//            if(name.equalsIgnoreCase("Homepage")){
//                loaduserfragment(new UserHomeFragment());
//            }
//        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;
        switch(item.getItemId()){
            case R.id.userHome:
                fragment = new UserHomeFragment();
                break;
            case R.id.userIntRequests:
                fragment = new UserRequestsFragment();
                break;
            case R.id.userInterviews:
                fragment = new UserInterviewsFragment();
                break;
            case R.id.userProfile:
                fragment = new UserProfileFragment();
                break;
        }
        return loaduserfragment(fragment);
    }
    private boolean loaduserfragment(Fragment fragment) {

        if(fragment != null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();
            return true;
        }
        return false;
    }
}