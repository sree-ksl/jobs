package com.ksl.jobs;

public class UpdateJobModel {

    String jobs, randomUid, description, vacancies, salary, imageUrl,emplrId;

    public UpdateJobModel(){}

    public String getJobs(){
        return jobs;
    }

    public void setJobs(String jobs){
        jobs = jobs;
    }

    public String getRandomUid(){
        return randomUid;
    }

    public void setRandomUid(String randomUid){
        randomUid = randomUid;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        description = description;
    }

    public String getVacancies(){
        return vacancies;
    }

    public void setVacancies(String vacancies){
        vacancies = vacancies;
    }

    public String getSalary(){
        return salary;
    }

    public void setSalary(String salary){
        salary = salary;
    }

    public String getImageUrl(){
        return imageUrl;
    }

    public void setImageUrl(String imageUrl){
        imageUrl = imageUrl;
    }

    public String getEmplrId(){
        return emplrId;
    }

    public void setEmplrId(String emplrId){
        emplrId = emplrId;
    }


}
