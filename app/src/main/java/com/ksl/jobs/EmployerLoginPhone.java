package com.ksl.jobs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.hbb20.CountryCodePicker;

public class EmployerLoginPhone extends AppCompatActivity {

    EditText emplrNumber;
    Button sendOtp, emailButton;
    TextView signUp;
    CountryCodePicker cpp;
    FirebaseAuth firebaseAuth;
    String eNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_login_phone_test);

        emplrNumber = findViewById(R.id.emplrNumber);
        sendOtp = findViewById(R.id.otp);
        emailButton = findViewById(R.id.btnEmail);
        signUp = findViewById(R.id.signup);
        cpp = findViewById(R.id.CountryCode);

        firebaseAuth = FirebaseAuth.getInstance();

        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eNumber = emplrNumber.getText().toString().trim();
                String phoneNum = cpp.getSelectedCountryCodeWithPlus()+eNumber;
                Intent b = new Intent(EmployerLoginPhone.this, EmployerSendOtp.class);
                b.putExtra("PhoneNum", phoneNum);
                startActivity(b);
                finish();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EmployerLoginPhone.this, EmployerRegister.class));
                finish();
            }
        });

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EmployerLoginPhone.this, EmployerLoginEmail.class));
                finish();
            }
        });

    }
}