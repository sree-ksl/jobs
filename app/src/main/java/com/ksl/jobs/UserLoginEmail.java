package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class UserLoginEmail extends AppCompatActivity {

    TextInputLayout userEmail, userPassword;
    Button uloginButton, uphoneButton;
    TextView uforgotPwd, usignUptext;
    FirebaseAuth firebaseAuth;
    String uEmail, uPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login_email_test);

        try {
            userEmail = findViewById(R.id.userEmail);
            userPassword = findViewById(R.id.userPwd);
            uloginButton = findViewById(R.id.userLoginBtn);
            uphoneButton = findViewById(R.id.ubtnphone);
            uforgotPwd = findViewById(R.id.forgotPwd);
            usignUptext = findViewById(R.id.uSignup);

            firebaseAuth = FirebaseAuth.getInstance();

            uloginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    uEmail = userEmail.getEditText().getText().toString().trim();
                    uPassword = userPassword.getEditText().getText().toString().trim();

                    if(isValid()){
                        final ProgressDialog mDialog = new ProgressDialog(UserLoginEmail.this);
                        mDialog.setCanceledOnTouchOutside(false);
                        mDialog.setCancelable(false);
                        mDialog.setMessage("Please wait.....");
                        mDialog.show();

                        firebaseAuth.signInWithEmailAndPassword(uEmail, uPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (task.isSuccessful()){
                                    mDialog.dismiss();

                                    if(firebaseAuth.getCurrentUser().isEmailVerified()){
                                        mDialog.dismiss();
                                        Toast.makeText(UserLoginEmail.this, "You are logged in!", Toast.LENGTH_SHORT).show();
                                        Intent z = new Intent(UserLoginEmail.this, UserPanel_BottomNav.class);
                                        startActivity(z);
                                        finish();
                                    } else{
                                        ReusableCodeforAll.showAlert(UserLoginEmail.this, "Verification Failed", "Please verify your email");
                                    }
                                } else{
                                    mDialog.dismiss();
                                    ReusableCodeforAll.showAlert(UserLoginEmail.this, "Error", task.getException().getMessage());

                                }
                            }
                        });
                    }
                }
            });

            usignUptext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(UserLoginEmail.this, UserRegister.class));
                    finish();
                }
            });

            uforgotPwd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(UserLoginEmail.this, UserForgotPassword.class));
                    finish();
                }
            });

            uphoneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(UserLoginEmail.this, UserLoginPhone.class));
                    finish();
                }
            });
        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

        }
    }



    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public boolean isValid(){

        userEmail.setErrorEnabled(false);
        userEmail.setError("");

        userPassword.setErrorEnabled(false);
        userPassword.setError("");

        boolean isValid = false, isValidEmail = false, isValidPwd = false;
        if(TextUtils.isEmpty(uEmail)){
            userEmail.setErrorEnabled(true);
            userEmail.setError("Email id is required");
        }else{
            if(uEmail.matches(emailPattern)){
                isValidEmail = true;
            } else{
                userEmail.setErrorEnabled(true);
                userEmail.setError("Invalid Email");
            }
        }

        if(TextUtils.isEmpty(uPassword)){
            userPassword.setErrorEnabled(true);
            userPassword.setError("Password Required");
        }
        else {
            isValidPwd = true;
        }

        isValid = (isValidEmail && isValidPwd)? true : false;
        return isValid;
    }
}