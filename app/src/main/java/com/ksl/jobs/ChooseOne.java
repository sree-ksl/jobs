package com.ksl.jobs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChooseOne extends AppCompatActivity {

    Button postJob, findJob;
    Intent intent;
    String type;
    ConstraintLayout bgImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_one);

//        AnimationDrawable animationDrawable = new AnimationDrawable();
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img1), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img2), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img3), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img4), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img5), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img6), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img7), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img8), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img9), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img10), 3000);
//        animationDrawable.addFrame(getResources().getDrawable(R.drawable.img11), 3000);
//
//        animationDrawable.setOneShot(false);
//        animationDrawable.setEnterFadeDuration(850);
//        animationDrawable.setExitFadeDuration(1600);

        bgImage = findViewById(R.id.choose);
//        bgImage.setBackgroundDrawable(animationDrawable);
//        animationDrawable.start();

        intent = getIntent();
        type = intent.getStringExtra("Home").toString().trim();

        postJob = findViewById(R.id.postJob);
        findJob = findViewById(R.id.findJob);

        postJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type .equals("Email")){
                    Intent loginEmail = new Intent(ChooseOne.this, EmployerLoginEmail.class);
                    startActivity(loginEmail);
                    finish();
                }
                if(type .equals("Phone")){
                    Intent loginPhone = new Intent(ChooseOne.this, EmployerLoginPhone.class);
                    startActivity(loginPhone);
                    finish();
                }
                if(type .equals("Signup")){
                    Intent register = new Intent(ChooseOne.this, EmployerRegister.class);
                    startActivity(register);

                }
            }
        });

        findJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type .equals("Email")){
                    Intent loginEmailUser = new Intent(ChooseOne.this, UserLoginEmail.class);
                    startActivity(loginEmailUser);
                    finish();
                }
                if(type .equals("Phone")){
                    Intent loginPhoneUser = new Intent(ChooseOne.this, UserLoginPhone.class);
                    startActivity(loginPhoneUser);
                    finish();
                }
                if(type .equals("Signup")){
                    Intent registerUser = new Intent(ChooseOne.this, UserRegister.class);
                    startActivity(registerUser);

                }
            }
        });

    }
}