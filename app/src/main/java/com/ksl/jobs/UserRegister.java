package com.ksl.jobs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;
import java.util.HashMap;

public class UserRegister extends AppCompatActivity {

    String AndhraPradesh[] = {"Vizag", "Vijayawada"};
    String Karnataka[] = {"Bangalore", "Mysore"};
    String Telangana[] = {"Hyderabad", "Secundrabad"};

    TextInputLayout userFirstName,userLastName,userEmail,userPassword,userConfirmPwd,userMobile,userCompany;
    Spinner userState,userCity;
    Button signup, email, phone;
    CountryCodePicker cpp;
    FirebaseAuth fAuth;
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;
    String uFirstName,uLastName,uEmail,uPassword,uConfirmPwd,uMobile,uCompany,uState, uCity;
    String role = "User";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);

        userFirstName = findViewById(R.id.userFirstName);
        userLastName = findViewById(R.id.userLastName);
        userEmail = findViewById(R.id.userEmail);
        userPassword = findViewById(R.id.userPwd);
        userConfirmPwd = findViewById(R.id.userConfirmPwd);
        userMobile = findViewById(R.id.userMobile);
        userCompany = findViewById(R.id.userCompany);
        userState = findViewById(R.id.stateSpinner);
        userCity = findViewById(R.id.citySpinner);

        signup = findViewById(R.id.userSignupBtn);
        email = findViewById(R.id.emailBtn);
        phone = findViewById(R.id.phoneBtn);

        cpp = findViewById(R.id.countryCode);

        userState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Object value = adapterView.getItemAtPosition(position);
                uState = value.toString().trim();
                if(uState.equals("AndhraPradesh")){
                    ArrayList<String> list = new ArrayList<>();
                    for (String cities : AndhraPradesh){
                        list.add(cities);
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UserRegister.this, android.R.layout.simple_spinner_item, list);
                    userCity.setAdapter(arrayAdapter);
                }
                if(uState.equals("Karnataka")){
                    ArrayList<String> list = new ArrayList<>();
                    for (String cities : Karnataka){
                        list.add(cities);
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UserRegister.this, android.R.layout.simple_spinner_item, list);
                    userCity.setAdapter(arrayAdapter);
                }
                if(uState.equals("Telangana")){
                    ArrayList<String> list = new ArrayList<>();
                    for (String cities : Telangana){
                        list.add(cities);
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UserRegister.this, android.R.layout.simple_spinner_item, list);
                    userCity.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        userCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Object value = adapterView.getItemAtPosition(position);
                uCity = value.toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference("User");
        fAuth = FirebaseAuth.getInstance();

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uFirstName = userFirstName.getEditText().getText().toString().trim();
                uLastName = userLastName.getEditText().getText().toString().trim();
                uEmail = userEmail.getEditText().getText().toString().trim();
                uPassword = userPassword.getEditText().getText().toString().trim();
                uConfirmPwd = userConfirmPwd.getEditText().getText().toString().trim();
                uMobile = userMobile.getEditText().getText().toString().trim();
                uCompany = userCompany.getEditText().getText().toString().trim();

                if (isValid()) {
                    final ProgressDialog mDialog = new ProgressDialog(UserRegister.this);
                    mDialog.setCancelable(false);
                    mDialog.setCanceledOnTouchOutside(false);
                    mDialog.setMessage("Registration in progress, please wait....");
                    mDialog.show();

                    fAuth.createUserWithEmailAndPassword(uEmail, uPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()){
                                String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                databaseReference = FirebaseDatabase.getInstance().getReference("user").child(userID);
                                final HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("Role", role);
                                databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        HashMap<String, String> hashMap1 = new HashMap<>();
                                        hashMap1.put("First Name", uFirstName);
                                        hashMap1.put("Last Name", uLastName);
                                        hashMap1.put("Emailid", uEmail);
                                        hashMap1.put("Password", uPassword);
                                        hashMap1.put("Confirm Password", uConfirmPwd);
                                        hashMap1.put("Mobile", uMobile);
                                        hashMap1.put("Company Name", uCompany);
                                        hashMap1.put("State", uState);
                                        hashMap1.put("City", uCity);

                                        firebaseDatabase.getInstance().getReference("User")
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                .setValue(hashMap1).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                mDialog.dismiss();

                                                fAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful()){
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(UserRegister.this);
                                                            builder.setMessage("Registration Successful. Please verify your Email");
                                                            builder.setCancelable(false);
                                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int which) {
                                                                    dialogInterface.dismiss();

                                                                    //phone num registration
                                                                    String phoneNumber = cpp.getSelectedCountryCodeWithPlus() + uMobile;
                                                                    Intent b = new Intent(UserRegister.this, UserVerifyPhone.class);
                                                                    b.putExtra("uphonenumber", phoneNumber);
                                                                    startActivity(b);
                                                                }
                                                            });
                                                            AlertDialog alert = builder.create();
                                                            alert.show();
                                                        } else{
                                                            mDialog.dismiss();
                                                            ReusableCodeforAll.showAlert(UserRegister.this,"Error",task.getException().getMessage());
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                            }else{
                                mDialog.dismiss();
                                ReusableCodeforAll.showAlert(UserRegister.this,"Error",task.getException().getMessage());
                            }
                        }
                    });
                }
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserRegister.this, UserLoginEmail.class));
                finish();
            }
        });

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserRegister.this, UserLoginPhone.class));
                finish();
            }
        });
    }

    //check for invalid entries
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public Boolean isValid(){
        userEmail.setErrorEnabled(false);
        userEmail.setError("");
        userFirstName.setErrorEnabled(false);
        userFirstName.setError("");
        userLastName.setErrorEnabled(false);
        userLastName.setError("");
        userPassword.setErrorEnabled(false);
        userPassword.setError("");
        userConfirmPwd.setErrorEnabled(false);
        userConfirmPwd.setError("");
        userMobile.setErrorEnabled(false);
        userMobile.setError("");
        userCompany.setErrorEnabled(false);
        userCompany.setError("");

        boolean isValid = false, isValidFirstname = false, isValidLastname = false, isValidemail = false, isValidPwd = false, isValidCpwd = false, isValidMobile = false, isValidCompany = false;
        if(TextUtils.isEmpty(uFirstName)){
            userFirstName.setErrorEnabled(true);
            userFirstName.setError("Enter First Name");
        }
        else {
            isValidFirstname = true;
        }

        if(TextUtils.isEmpty(uLastName)){
            userLastName.setErrorEnabled(true);
            userLastName.setError("Enter Last Name");
        }
        else {
            isValidLastname = true;
        }

        if(TextUtils.isEmpty(uEmail)){
            userEmail.setErrorEnabled(true);
            userEmail.setError("Email id is required");
        }
        else {
            if(uEmail.matches(emailPattern)){
                isValidemail = true;
            } else{
                userEmail.setErrorEnabled(true);
                userEmail.setError("Enter valid email id");
            }

        }

        if(TextUtils.isEmpty(uPassword)){
            userPassword.setErrorEnabled(true);
            userPassword.setError("Enter Password");
        }
        else {
            if (uPassword.length()<8){
                userPassword.setErrorEnabled(true);
                userPassword.setError("Weak Password");
            }
            else{
                isValidPwd = true;
            }
        }

        if(TextUtils.isEmpty(uConfirmPwd)){
            userConfirmPwd.setErrorEnabled(true);
            userConfirmPwd.setError("Enter password again");
        } else {
            if(!uPassword.equals(uConfirmPwd)){
                userConfirmPwd.setErrorEnabled(true);
                userConfirmPwd.setError("Mismatched password");
            }
            else{
                isValidCpwd = true;

            }
        }

        if(TextUtils.isEmpty(uMobile)){
            userMobile.setErrorEnabled(true);
            userMobile.setError("Enter Mobile number");
        }
        else {
            if(uMobile.length()<10){
                userMobile.setErrorEnabled(true);
                userMobile.setError("Invalid Mobile number");
            }
            else{
                isValidMobile = true;
            }
        }

        if(TextUtils.isEmpty(uCompany)){
            userCompany.setErrorEnabled(true);
            userCompany.setError("Enter Company Name");
        }
        else {
            isValidCompany = true;
        }


        isValid = (isValidFirstname && isValidLastname && isValidemail && isValidPwd && isValidCpwd && isValidCompany && isValidMobile) ? true : false;
        return isValid;
    }
}