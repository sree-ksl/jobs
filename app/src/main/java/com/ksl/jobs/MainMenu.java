package com.ksl.jobs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class MainMenu extends AppCompatActivity {

    Button emailSignin, phoneSignin, signUp;
    ImageView bgImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        final Animation zoomIn = AnimationUtils.loadAnimation(this, R.anim.zoomin);
        final Animation zoomOut = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        bgImage = findViewById(R.id.background);
        bgImage.setAnimation(zoomIn);
        bgImage.setAnimation(zoomOut);

        zoomOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bgImage.startAnimation(zoomIn);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        zoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bgImage.startAnimation(zoomOut);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        emailSignin = findViewById(R.id.EmailSignup);
        phoneSignin = findViewById(R.id.PhoneSignup);
        signUp = findViewById(R.id.signup);

        emailSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailSignin = new Intent(MainMenu.this, ChooseOne.class);
                emailSignin.putExtra("Home", "Email");
                startActivity(emailSignin);
                finish();
            }
        });

        phoneSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent phoneSignin = new Intent(MainMenu.this, ChooseOne.class);
                phoneSignin.putExtra("Home", "Phone");
                startActivity(phoneSignin);
                finish();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signup = new Intent(MainMenu.this, ChooseOne.class);
                signup.putExtra("Home", "Signup");
                startActivity(signup);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }
}